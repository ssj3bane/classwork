package tcayer.improvements;

public class ErrPrint {
	public static void ln(Object arg0) {
		System.err.println(arg0);
	}
	public static void ln(Object[] arg0) {
		System.err.println(arg0);
	}
	public static void p(Object arg0) {
		System.err.print(arg0);
	}
	public static void p(Object[] arg0) {
		System.err.print(arg0);
	}
}
