package tcayer.improvements;

public class Print {
	public static void ln(Object arg0) {
		System.out.println(arg0);
	}
	public static void ln(Object[] arg0) {
		System.out.println(arg0);
	}
	public static void p(Object arg0) {
		System.out.print(arg0);
	}
	public static void p(Object[] arg0) {
		System.out.print(arg0);
	}
}