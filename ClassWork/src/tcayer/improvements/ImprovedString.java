package tcayer.improvements;

@SuppressWarnings("hiding")
public class ImprovedString {
	public String value;
	public ImprovedString() {
		this.value = "";
	}
	public ImprovedString(char singleChar) {
		this.value = "" + singleChar;
	}
	public ImprovedString(String value) {
		this.value = value;
	}
	public char lastChar() {
		return this.value.charAt(this.value.length()-1);
	}
	public int length() {
		return this.value.length();
	}
	public String toString() {
		return this.value.toString();
	}
	public boolean equals(Object anObject) {
		return this.value.equals(anObject);
	}
}