package tcayer.homework;

import java.util.Scanner;

public class Homework4 {
	public static Scanner kb;
	public static String answer = "";
	public static String[] boxCharacters = new String[] {
			"▉",		//0
			"▖",		//1
			"▗",		//2
			"▘",		//3
			"▙",		//4
			"▚",		//5
			"▛",		//6
			"▜",		//7
			"▝",		//8
			"▞",		//9
			"▟"		//10
	};
	//6 Bottom
	//4 Top
	//0 Center
	public static void main(String[] args) {
		kb = new Scanner(System.in);
		answer = "";
		while(!answer.toLowerCase().equals("QUIT")) {
			intro();
			if(answer.equals("1")) {
				problem1();
			} else if(answer.equals("2")) {
				problem2();
			}
		}
		kb.close();
	}
	public static void intro() {
		System.out.println("Which problem are you viewing?\n"
				+ "Problem 1: Square root.\n"
				+ "Problem 2: Triangle size.\n"
				+ "QUIT: Type QUIT to exit the program.\n"
				+ "(Valid inputs are 1, 2, and QUIT)");
		answer = kb.nextLine();
	}
	public static void problem1() {
		int numToSqrt = -1;
		while(numToSqrt != 0) {
			System.out.print("Enter 0 to exit.\nEnter a number: ");
			numToSqrt = kb.nextInt();
			if(numToSqrt > 0) {
				System.out.println("The square root of "
						+ numToSqrt + " is " + Math.sqrt(numToSqrt));
			} else if(numToSqrt != 0) {
				System.out.println("Number must be positive");
			}
		}
	}
	public static void problem2() {
		while(!answer.toLowerCase().equals("quit")) {
			System.out.println("Enter QUIT to exit.\nWould you like to use a fancy view? Y/N");
			answer = kb.nextLine();
			if(answer.toUpperCase().equals("Y") || answer.toUpperCase().equals("N")) {
				System.out.println("Input the size of the triangle:");
				int size = kb.nextInt();
				if(answer.toUpperCase().equals("Y")) {
					//Fancy mode
					for(int i = 0; i < (size*2)+1; i++) {
						int charCount = size-Math.abs((size)-i);
						if(charCount != 0) {
							String newLine = "";
							for(int j = 1; j <= charCount; j++) {
								if(j != charCount) {
									newLine += boxCharacters[0];
								} else {
									if((size-i) > 0) {
										newLine += boxCharacters[4];
									} else if((size-i) < 0) {
										newLine += boxCharacters[6];
									} else {
										newLine += boxCharacters[0];
									}
								}
							}
							System.out.println(newLine);
						}
					}
				} else {
					//Normal mode
					for(int i = 0; i < (size*2)+1; i++) {
						int charCount = size-Math.abs((size)-i);
						if(charCount != 0) {
							String newLine = "";
							for(int j = 1; j <= charCount; j++) {
								newLine += "*";
							}
							System.out.println(newLine);
						}
					}
				}
			}
		}
	}
}