package tcayer.homework;

public class Conversion {
	private static final String[] replaceOrderFrom = new String[] {
			"DCCCC",
			"CCCC",
			"LXXXX",
			"XXXX",
			"VIIII",
			"IIII"
	};
	private static final String[] replaceOrderTo = new String[] {
			"CM",
			"CD",
			"XC",
			"XL",
			"IX",
			"IV"
	};
	public static void flightCheck() {
		if(replaceOrderFrom.length != replaceOrderTo.length) {
			System.err.println("REPLACE ARRAYS ARE NOT OF EQUAL LENGTH!");
		}
	}
	/**
	 * 
	 * @param Integer number
	 * @param Integer div
	 * @param St-ring letter
	 * @return Object
	 * 			Object[0] = String of a repeated string
	 * 			Object[1] = Integer of the remainder for the Roman numeral
	 */
	private static Object[] roman(int number, int div, String letter) {
		Object[] rv = new Object[2];
		int remainder = number%div;
		int letterCount = (number-remainder) / div;
		String repLetter = repeat(letter, letterCount);
		rv[0] = repLetter;
		rv[1] = remainder;
		return rv;
	}
	/**
	 * Converts an arabic number into a string representing a Roman numeral.
	 * @param arabic - An int
	 * @return String representing a Roman numeral of the input
	 */
	public static String convert(int arabic) {
		String romanNumeral = "";
		Object[] M = roman(arabic, 1000, "M");
		romanNumeral += M[0];
		Object[] D = roman((int)M[1], 500, "D");
		romanNumeral += D[0];
		Object[] C = roman((int)D[1], 100, "C");
		romanNumeral += C[0];
		Object[] L = roman((int)C[1], 50, "L");
		romanNumeral += L[0];
		Object[] X = roman((int)L[1], 10, "X");
		romanNumeral += X[0];
		Object[] V = roman((int)X[1], 5, "V");
		romanNumeral += V[0];
		romanNumeral += repeat("I", (int)V[1]);
		// Replace section
		for(int i = 0; i < replaceOrderFrom.length; i++)
			romanNumeral = romanNumeral.replaceAll(replaceOrderFrom[i], replaceOrderTo[i]);
		return romanNumeral;
	}
	/**
	 * Converts a string representing a Roman numeral into an integer.
	 * @param roman - A string representing a Roman numeral
	 * @return Integer equal to the value of the Roman numeral
	 */
	public static int convert(String roman) {
		int arabic = 0;
		for(int i = replaceOrderFrom.length-1; i >= 0; i--)
			roman = roman.replaceAll(replaceOrderTo[i], replaceOrderFrom[i]);
		arabic += 1000 * charCount(roman, 'M');
		arabic += 500 * charCount(roman, 'D');
		arabic += 100 * charCount(roman, 'C');
		arabic += 50 * charCount(roman, 'L');
		arabic += 10 * charCount(roman, 'X');
		arabic += 5 * charCount(roman, 'V');
		arabic += charCount(roman, 'I');
		return arabic;
	}
	private static String repeat(String string, int amt) {
		String rv = "";
		for(int i = 0; i < amt; i++)
			rv+=string;
		return rv;
	}
	private static int charCount(String string, char ch) {
		int rv = 0;
		for(int i = 0; i < string.length(); i++)
			if(string.charAt(i) == ch)
				rv++;
		return rv;
	}
}