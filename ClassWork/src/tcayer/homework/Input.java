package tcayer.homework;

import java.util.Scanner;

public class Input {
	public static String promptString(String string, Scanner input) {
		System.out.println(string);
		return input.next();
	}
	public static int promptInt(String string, Scanner input) {
		System.out.println(string);
		return input.nextInt();
	}
	public static void promptRoman(Scanner kb) {
		System.out.println("Enter a Roman numeral:");
		Print.print(kb.next());
	}
	public static void promptNumber(Scanner kb) {
		System.out.println("Enter a number:");
		Print.print(kb.nextInt());
	}
}