package tcayer.homework;

public class Print {
	public static void print(int preNumeral) {
		String postNumeral = Conversion.convert(preNumeral);
		System.out.println("The number " + preNumeral + " as a Roman numeral is " + postNumeral + ".");
	}
	public static void print(String postNumeral) {
		int preNumeral = Conversion.convert(postNumeral);
		System.out.println("The Roman numeral " + postNumeral + " is equal to " + preNumeral + ".");
	}
}
