package tcayer.homework;

import java.util.Scanner;
// You may need to adjust/add the import due to the package changing
// after uploading, as it is unnecessary when used in the same
// package.
public class Homework6 {
	private static final String PROMPT_STRING = "Enter NUMBER to convert a number to a Roman numeral\nEnter ROMAN to convert a Roman numeral to a number\nEnter QUIT to quit";
	public static void main(String[] args) {
		Conversion.flightCheck();
		neatNTidy();
	}
	private static void neatNTidy(){
		Scanner kb = new Scanner(System.in);
		String reply = Input.promptString(PROMPT_STRING,kb);
		while(!reply.toLowerCase().equals("quit")) {
			if(reply.toLowerCase().equals("number"))
				Input.promptNumber(kb);
			if(reply.toLowerCase().equals("roman"))
				Input.promptRoman(kb);
			reply = Input.promptString(PROMPT_STRING,kb);
		}
		kb.close();
	}
}

