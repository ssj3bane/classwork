package tcayer.lab;

import java.util.Scanner;

public class Lab5P1 {
	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		compactingFunctions(keyboard);
	}
	// Tidy up main() by putting all the junky functions into a single method
	public static void compactingFunctions(Scanner input) {
		// Prompt for the first number
		System.out.println("Enter the first number: ");
		double numberOne = input.nextDouble();
		// Prompt for the operator
		System.out.println("Enter a valid operator(+,-,*,/,%): ");
		String operator = input.next();
		// Prompt for the second number
		System.out.println("Enter the second number: ");
		double numberTwo = input.nextDouble();
		// Calculate the numbers with the operator
		double answer = calculate(numberOne, operator, numberTwo);
		System.out.println(answer);
	}
	// Quick calculating method
	public static double calculate(double firstNumber, String operation, double secondNumber) {
		// Initialize the result with a value of 0.0
		double result = 0.0;
		// Switch case for all operations with an error if
		// given an invalid operator
		switch(operation) {
			case "+":
				result = firstNumber + secondNumber;
				break;
			case "-":
				result = firstNumber - secondNumber;
				break;
			case "*":
				result = firstNumber * secondNumber;
				break;
			case "/":
				if(secondNumber == 0.0) {
					throw new Error("Cannot divide by zero.");
				}
				result = firstNumber / secondNumber;
				break;
			case "%":
				result = firstNumber % secondNumber;
				break;
			default:
				throw new Error("Operation must be one of the following: +, -, *, /, %.");
		}
		// Return the result
		return result;
	}
}