package tcayer.lab;

import java.util.Scanner;
import java.util.Calendar;
import java.util.Locale;
public class Lab5P2 {
	// Create an integer array with the days of the month
	public static final int[] DAY_COUNT = new int[] {
		31,
		28,
		31,
		30,
		31,
		30,
		31,
		31,
		30,
		31,
		30,
		31
	};
	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		compactingFunctions(keyboard);
	}
	// Tidy up main() by putting all the junky functions into a single method
	public static void compactingFunctions(Scanner input) {
		int year = input.nextInt();
		int month = input.nextInt();
		// This section is just an "easier" way to get the name of the month
		Calendar testDate = Calendar.getInstance();
		// Set the Calendar with the year, month, and a default day of 1
		testDate.set(year, month-1, 1);
		// Get the month name from the Calendar
		String monthName = testDate.getDisplayName(2, testDate.LONG, Locale.ENGLISH);
		// Get the amount of days to be added in the case of a leap year
		int daysToAdd = calculate(month, year);
		System.out.println(testDate);
		System.out.println("The month is " + monthName);
		System.out.println("The number of days is " + (DAY_COUNT[month-1] + daysToAdd));
	}
	// Leap year calculation
	public static int calculate(int month, int year) {
		// Initialize daysToAdd with a default of 0 to avoid
		// excessive else statements
		int daysToAdd = 0;
		// Only bother if it's February
		if(month == 2) {
			// Check if the year is event divisible by 4
			if(year % 4 == 0) {
				// Check if the year is evenly divisible by 100
				if(year % 100 == 0) {
					// Check if the year is evenly divisible by 400
					if(year % 400 == 0) {
						daysToAdd = 1;
					}
				} else {
					daysToAdd = 1;
				}
			}
		}
		return daysToAdd;
	}
}