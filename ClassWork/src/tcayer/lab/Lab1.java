package tcayer.lab;

import java.util.Scanner;
@SuppressWarnings("unused")
public class Lab1
{
	public static final float FEET_IN_CUBIC_YARD = 27;
	public static void main(String[] args)
	{
		/*
		 * Declarations
		 */
		float 	sideLength, plantSpacing, gardenDepth, fillDepth,
				circleGardenArea, semicircleGardenArea, gardenTotalArea, gardenVolume, flowersInSemiCircle, flowersInCircle, totalFlowers,
				soilSemiCircle, soilCircle, totalSoil, totalFill;
		Scanner keyboard = new Scanner(System.in);
		/*
		 * Declarations end
		 */
		/*
		 * Prompts
		 */
		sideLength = askForFloat("Enter the length of the side of the garden: ", keyboard);
		plantSpacing = askForFloat("Enter the spacing between plants: ",  keyboard);
		gardenDepth = askForFloat("Enter the depth of the garden soil: ", keyboard);
		fillDepth = askForFloat("Enter the depth of the fill: ", keyboard);
		/*
		 * Prompts end
		 */
		/*
		 * Calculations
		 */
		circleGardenArea = areaCircle(sideLength / 4);
		semicircleGardenArea = circleGardenArea / 2;
		gardenTotalArea = areaRectangle(sideLength, sideLength);
		gardenVolume = gardenTotalArea * fillDepth;
		flowersInSemiCircle = pointsInCircle(circleGardenArea, plantSpacing);
		flowersInCircle = flowersInSemiCircle * 2;
		totalFlowers = (flowersInCircle) + (flowersInSemiCircle * 4);
		soilSemiCircle = semicircleGardenArea * gardenDepth;
		soilCircle = circleGardenArea * gardenDepth;
		totalSoil = (soilSemiCircle * 4) + soilCircle;
		totalFill = gardenVolume - totalSoil;
		/*
		 * Calculations end
		 */
		/*
		 * Display
		 */
		System.out.println("Plants in each semicircle garden: " +  flowersInSemiCircle);
		System.out.println("Plants in each circle garden: " + flowersInCircle);
		System.out.println("Total plants for each garden: " + totalFlowers);
		System.out.println("Soil for each semicircle garden: " + (soilSemiCircle / FEET_IN_CUBIC_YARD));
		System.out.println("Soil for the circle garden: " + (soilCircle / FEET_IN_CUBIC_YARD));
		System.out.println("Total soil for the garden: " + (totalSoil / FEET_IN_CUBIC_YARD));
		System.out.println("Total fill for the garden: " + (totalFill / FEET_IN_CUBIC_YARD));
		/*
		 * Display end
		 */
		keyboard.close();
	}
	/**
	 * Space saving keyboard input
	 * @param prompt
	 * @param input
	 * @return float
	 */
	public static float askForFloat(String prompt, Scanner input)
	{
		System.out.print(prompt);	//Print a line functioning as a prompt
		return input.nextFloat();	//Return the user input
	}
	/**
	 * Approximate the number of points in a circle with a given space limiter
	 * @param rawArea
	 * @param spacing
	 * @return float
	 */
	public static float pointsInCircle(float rawArea, float spacing)
	{
		float returnValue;									//Define what to return
		returnValue = (float) Math.round(rawArea/spacing);	//Approximate how many points are in a circle with the given parameters
		return returnValue;									//Return the approximation of points in a circle with a defined spacing
	}
	/**
	 * Calculate the area of a circle with a given radius
	 * @param radius
	 * @return
	 */
	public static float areaCircle(float radius)
	{
		float returnValue;
		returnValue  = (float) (Math.PI * Math.pow(radius, 2));
		return returnValue;
	}
	/**
	 * Calculate the area of a rectangle with the given side lengths
	 * @param side1
	 * @param side2
	 * @return
	 */
	public static float areaRectangle(float side1, float side2)
	{
		float returnValue;
		returnValue  = (float) (side1 * side2);
		return returnValue;
	}
}