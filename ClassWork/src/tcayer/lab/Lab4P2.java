package tcayer.lab;

import java.util.Scanner;

public class Lab4P2 {
	public static void main(String[] args) {
		int booksPurchased, bookPoints;
		
		// Define a scanner with the IDE input
		Scanner KB = new Scanner(System.in);
		System.out.println("How many books have you purchased this month?");
		booksPurchased = KB.nextInt();
		if(booksPurchased >= 4) {
			bookPoints = 60;
		} else if(booksPurchased >= 3) {
			bookPoints = 30;
		} else if(booksPurchased >= 2) {
			bookPoints = 15;
		} else if(booksPurchased >= 1) {
			bookPoints = 5;
		} else {
			bookPoints = 0;
		}
		System.out.println("You have " + bookPoints + " points.");
		KB.close();
	}
}