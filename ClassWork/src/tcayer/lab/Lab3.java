package tcayer.lab;

import java.util.Scanner;

public class Lab3 {
	public static void main(String[] args) {
		// Define a scanner with the IDE input
		Scanner KB = new Scanner(System.in);
		// Define the first string to compare string2 to
		String string1 = KB.nextLine();
		// Define the second string to compare string1 to
		String string2 = KB.nextLine();
		// Print the comparison results
		System.out.println(compareStrings(string1,string2));
		// Define the string to be tested
		String strOrig = KB.nextLine();
		// Define the substring
		String Stringname = KB.nextLine();
		// Print the index of the character before the last index of the substring
		System.out.println(secondToLast(strOrig, Stringname));
		KB.close();
	}
	public static boolean compareStrings(String string1, String string2) {
		System.out.println("Comparing " + string1 + " with " + string2);
		return string1.compareTo(string2)==0;
		// String comparison returns 0 when true, convert that to boolean
	}
	public static int secondToLast(String strOrig, String Stringname) {
		// Define secondToLastPos as the last index of the substring on the string
		int secondToLastPos = strOrig.lastIndexOf(Stringname) - 1;
		// Get the character at the index of secondToLastPos in the string
		char secondToLastChar = strOrig.charAt(secondToLastPos);
		System.out.println(secondToLastChar);
		return secondToLastPos;
		// Return the index of the character
	}
}