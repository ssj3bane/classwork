package tcayer.lab;

import java.util.Scanner;

import tcayer.improvements.Print;
import tcayer.improvements.ErrPrint;

public class Lab6 {
	public static void main(String[] args) {
		StudentGrade tim = new StudentGrade();
		tim.setName();
		tim.displayName();
		tim.getData();
		tim.calculateGrades();
		tim.displayPercentGrade();
		tim.displayLetterGrade();
	}
}

class StudentGrade {
	private String name;
	private int[] QUIZ_GRADES = new int[2];
	private int FINAL_QuizGrade;
	@SuppressWarnings("unused")
	private boolean SET_QuizGrade = false;
	private int FINAL_MidtermGrade;
	@SuppressWarnings("unused")
	private boolean SET_MidtermGrade = false;
	private int FINAL_FinalGrade;
	@SuppressWarnings("unused")
	private boolean SET_FinalGrade = false;
	private int FINAL_GRADE;
	@SuppressWarnings("unused")
	private boolean Calculated_FINAL_GRADE = false;
	
	public StudentGrade(String name) {
		this.name = name;
	}
	public StudentGrade() {
		
	}
	public void getData() {
		Scanner input = new Scanner(System.in);
		Print.ln("Enter 2 quiz scores");
		for(int i = 0; i < QUIZ_GRADES.length; i++) {
			boolean validInput = true;
			QUIZ_GRADES[i] = (int) prompt("Quiz " + (i+1) + ": ", input, 1);
			if(QUIZ_GRADES[i] < 0 || QUIZ_GRADES[i] > 10) {
				ErrPrint.ln("GRADE INVALID");
				Print.ln("");
				validInput = false;
			}
			while(!validInput) {
				QUIZ_GRADES[i] = (int) prompt("Quiz " + (i+1) + ": ", input, 1);
				if(QUIZ_GRADES[i] < 0 || QUIZ_GRADES[i] > 10) {
					ErrPrint.ln("GRADE INVALID");
					Print.ln("");
					validInput = false;
				} else {
					validInput = true;
				}
			}
		}
		this.SET_QuizGrade = true;
		this.FINAL_MidtermGrade = (int) prompt("Enter midterm grade: ", input, 1);
		boolean validInput = true;
		if(this.FINAL_MidtermGrade < 0 || this.FINAL_MidtermGrade > 100) {
			ErrPrint.ln("GRADE INVALID");
			Print.ln("");
			validInput = false;
		}
		while(!validInput) {
			this.FINAL_MidtermGrade = (int) prompt("Enter midterm grade: ", input, 1);
			if(this.FINAL_MidtermGrade < 0 || this.FINAL_MidtermGrade > 100) {
				ErrPrint.ln("GRADE INVALID");
				Print.ln("");
				validInput = false;
			} else {
				validInput = true;
			}
		}		
		this.SET_MidtermGrade =  true;
		this.FINAL_FinalGrade = (int) prompt("Enter final grade: ", input, 1);
		validInput = true;
		if(this.FINAL_FinalGrade < 0 || this.FINAL_FinalGrade > 100) {
			ErrPrint.ln("GRADE INVALID");
			validInput = false;
		}
		while(!validInput) {
			this.FINAL_FinalGrade = (int) prompt("Enter final grade: ", input, 1);
			if(this.FINAL_FinalGrade < 0 || this.FINAL_FinalGrade > 100) {
				ErrPrint.ln("GRADE INVALID");
				validInput = false;
			} else {
				validInput = true;
			}
		}
		this.SET_FinalGrade =  true;
	}
	private Object prompt(String _prompt, Scanner input, Object type) {
		Print.p(_prompt);
		Object rv = new Error();
		boolean t = true;
		if(type instanceof Integer)
			if(input.hasNextInt())
				rv = input.nextInt();
		else if(type instanceof String)
			if(input.hasNext())
				rv = input.next();
		else if(type instanceof Boolean)
			if(input.hasNextBoolean())
				rv = input.nextBoolean();
		else if(type instanceof Float)
			if(input.hasNextFloat())
				rv = input.nextFloat();
		while(rv instanceof Error && t) {
			if(type instanceof Integer)
					rv = input.nextInt();
			else if(type instanceof String)
					rv = input.next();
			else if(type instanceof Boolean)
					rv = input.nextBoolean();
			else if(type instanceof Float)
					rv = input.nextFloat();
			t = false;
		}
		return rv;
	}
	
	public String getName() {
		return name;
	}
	public void setName() {
		Scanner input = new Scanner(System.in);
		this.name = (String) prompt("Enter your name: ", input, "");
	}
	public void displayName() {
		Print.ln("Student name: " + this.name);
	}
	
	public void setQuiz2Grade() {
		Scanner input = new Scanner(System.in);
		this.QUIZ_GRADES[1] = (int) prompt("Quiz 2: ", input, 1);
		
	}
	public int returnQuiz2Grade() {
		return this.QUIZ_GRADES[1];
	}
	public void displayQuiz2Grade() {
		Print.ln("Quiz 2 grade: " + this.QUIZ_GRADES[1]);
	}
	
	public void setMidtermGrade() {
		Scanner input = new Scanner(System.in);
		this.FINAL_MidtermGrade = (int) prompt("Midterm: ", input, 1);
		
	}
	public int returnMidtermGrade() {
		return this.FINAL_MidtermGrade;
	}
	public void displayMidtermGrade() {
		Print.ln("Midterm grade: " + this.FINAL_MidtermGrade);
	}
	
	public void setFinalGrade() {
		Scanner input = new Scanner(System.in);
		this.FINAL_FinalGrade = (int) prompt("Final: ", input, 1);
		
	}
	public int returnFinalGrade() {
		return this.FINAL_FinalGrade;
	}
	public void displayFinalGrade() {
		Print.ln("Final Exam: " + this.FINAL_FinalGrade);
	}
	
	public void calculateGrades() {
		// Calculate Quiz Grades
		int QuizGrade = 0;
		for(int grade : this.QUIZ_GRADES)
			QuizGrade += (grade * 10);
		QuizGrade /= this.QUIZ_GRADES.length;
		this.FINAL_QuizGrade = QuizGrade;
		this.FINAL_GRADE = (this.FINAL_QuizGrade +
							this.FINAL_MidtermGrade +
							(this.FINAL_FinalGrade * 2)) / 4;
		this.Calculated_FINAL_GRADE = true;
		
	}
	
	public int returnPercentGrade() {
		return this.FINAL_GRADE;
	}
	public void displayPercentGrade() {
		Print.ln("Final grade: " + this.FINAL_GRADE);
	}
	
	public char returnLetterGrade() {
		char grade;
		if(this.FINAL_GRADE >= 90) {
			grade = 'A';
		} else if(this.FINAL_GRADE >= 80) {
			grade = 'B';
		} else if(this.FINAL_GRADE >= 70) {
			grade = 'C';
		} else if(this.FINAL_GRADE >= 60) {
			grade = 'D';
		} else {
			grade = 'F';
		}
		return grade;
	}
	public void displayLetterGrade() {
		Print.ln("Final grade: " + returnLetterGrade());
	}
	
	
}

