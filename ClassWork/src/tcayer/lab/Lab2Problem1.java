package tcayer.lab;

public class Lab2Problem1 {
	public static void main(String[] args) {
		firstProblem(); // Do the first problem
	}
	
	public static int[] separateInt(int input) {
		// Define a string array for the input to separated into
		String[] inputString = Integer.toString(input).split("");
		// Define the return value as an integer array with a length the same as the string array
		int[] returnValue = new int[inputString.length];
		// Define a counter to help determine the place a new number should be added into the integer array
		int counter = 0;
		// Loop for the length of the string array
		for(String num : inputString) {
			// Set the value at its respective place in the integer array
			returnValue[counter] = Integer.parseInt(num);
			// Increase the counter by 1
			counter++;
		}
		// Return the integer array
		return returnValue;
	}
	
	public static int multiplyIntArray(int[] intArray) {
		// Define the return value as an int
		int returnValue = 0;
		// Loop for each number in the integer  array
		for(int num : intArray) {
			// If the return value hasn't been changed, set it to the first number in the integer array, otherwise multiply it by the integer currently being looped
			if(returnValue != 0) {
				returnValue *= num;
			} else {
				returnValue = num;
			}
		}
		// Return the return value
		return returnValue;
	}
	
	public static void firstProblem() {
		System.out.println("START Problem 1");
		// Define a random number to use
		int randomNumber = (int) Math.round(Math.random() * 999);
		// Separate the random number into an integer array
		int[] separatedNumber = separateInt(randomNumber);
		System.out.println("Random number: " + randomNumber);
		// Set the answer, refer to the multiplyIntArray function to see what it does
		int answer = multiplyIntArray(separatedNumber);
		System.out.println("Answer: " + answer);
		System.out.println("END Problem 1");
	}
}