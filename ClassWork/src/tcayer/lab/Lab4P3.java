package tcayer.lab;

import java.util.Scanner;

public class Lab4P3 {
	public static boolean isFixed = false;
	public static boolean reachedEndOfSolution = false;
	public static int counter = 0;
	public static final String[] SOLUTIONS = new String[] {
		"Reboot the computer and try to connect.",
		"Reboot the router and try to connect.",
		"Make sure the cables between the router and modem are\nplugged in firmly.",
		"Move the router to a new location."
	};
	// Define a scanner with the IDE input
	public static Scanner KB = new Scanner(System.in);
	public static void main(String[] args) {
		// While loop until the issue is resolved
		while(!isFixed) {
			// If out of solutions, get a new router
			if(reachedEndOfSolution) {
				System.out.println("Get a new router.");
				// Break the while loop
				break;
			}
			// Try to solve the issue
			routerFix();
		}
	}
	public static void routerFix() {
		// If the current solution number exceeds the total solutions
		// mark the end of solution variable as true
		if(counter > SOLUTIONS.length - 1) { 
			reachedEndOfSolution = true;
			// CLOSE THE KEYBOARD, IT'S SUPPOSED TO BE CLOSED AFTER USE
			KB.close();
		} else {
			// Print the solution for the current solution number
			System.out.println(SOLUTIONS[counter]);
			// Ask if the problem is solved
			System.out.println("Did that fix the problem?");
			String answer = KB.nextLine();
			// Who needs case checking when it can be lowercase?
			answer = answer.toLowerCase();
			if(answer.compareTo("yes") == 0) {
				// If they said yes, it's fixed
				isFixed = true;
			}
			counter++;
		}
	}
}