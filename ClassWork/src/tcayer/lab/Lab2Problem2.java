package tcayer.lab;

public class Lab2Problem2 {
	public static void main(String[] args) {
		secondProblem();// Do the second problem
	}
	
	public static int[] separateInt(int input) {
		// Define a string array for the input to separated into
		String[] inputString = Integer.toString(input).split("");
		// Define the return value as an integer array with a length the same as the string array
		int[] returnValue = new int[inputString.length];
		// Define a counter to help determine the place a new number should be added into the integer array
		int counter = 0;
		// Loop for the length of the string array
		for(String num : inputString) {
			// Set the value at its respective place in the integer array
			returnValue[counter] = Integer.parseInt(num);
			// Increase the counter by 1
			counter++;
		}
		// Return the integer array
		return returnValue;
	}
	
	public static String intArrayToString(int[] intArray) {
		// Create an empty string
		String returnValue = "";
		// Loop for each number in an integer array
		for(int num : intArray) {
			// Just append the current integer to the end of the return value
			returnValue += num;
		}
		// Return the return value
		return returnValue;
	}
	
	public static void secondProblem() {
		System.out.println("START Problem 2");
		// Define a random number to use
		int randomNumber = (int) Math.round(Math.random() * 999999);
		// Separate the random number into an integer array
		int[] separatedNumber = separateInt(randomNumber);
		System.out.println("Random number: " + randomNumber);
		// Set the answer, refer to the intArrayToString function to see what it does... Simply join the string array with 2 pound signs
		String answer = String.join("##",intArrayToString(separatedNumber).split(""));
		System.out.println("Answer: " + answer);
		System.out.println("END Problem 2");
	}
}