package tcayer.lab;

public class Lab4P1 {
	public static final double TOTAL_CUSTOMERS = 12467;
	public static final double BOUGHT_MORE_THAN_ONE = 0.14;
	public static final double LIKES_CITRUS = 0.64;
	public static void main(String[] args) {
		System.out.println((TOTAL_CUSTOMERS * BOUGHT_MORE_THAN_ONE) + " customers purchased one or more energy drinks per week.");
		System.out.println(((TOTAL_CUSTOMERS * BOUGHT_MORE_THAN_ONE) * LIKES_CITRUS) + " customers enjoy citrus energy drinks.");
	}
}