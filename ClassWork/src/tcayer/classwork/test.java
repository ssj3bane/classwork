package tcayer.classwork;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import javax.script.*;
import tcayer.improvements.ImprovedString;

@SuppressWarnings("unused")
public class test {
	public static void main(String[] args) throws FileNotFoundException, ScriptException {
		System.out.println("Supply a file");
		Scanner kb = new Scanner(System.in);
		//C:\Users\timom\Desktop\TestFiles\mockJavascript.txt
		File file = new File(kb.nextLine());//"C:\\Users\\timom\\Desktop\\TestFiles\\mockJavascript.txt"
		kb.close();
		Scanner sc = new Scanner(file);
		String builtScript = "";
		while(sc.hasNextLine()) {
			builtScript += sc.nextLine() + "\n";
		}
		ScriptEngine jsEngine = createEngine("js");
		runScript(jsEngine, builtScript);
		sc.close();
	}
	public static void runScript(ScriptEngine engine, String script) throws ScriptException {
		System.out.println("Running script:\n"+script);
		Object result = engine.eval(script);
		System.out.println("Result:\n"+result);
	}
	public static ScriptEngine createEngine(String engineName) {
		ScriptEngineManager manager = new ScriptEngineManager();
		ScriptEngine engine = manager.getEngineByName(engineName);
		System.out.println("Constructed engine: " + engineName);
		return engine;
	}
}
