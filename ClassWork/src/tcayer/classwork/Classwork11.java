package tcayer.classwork;

import java.util.Scanner;

import tcayer.improvements.ErrPrint;
import tcayer.improvements.Print;
import tcayer.improvements.Prompt;

public class Classwork11 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		Dog somePet = new Dog("No owner yet", "Jane Doe");
		
		Pet myPet = somePet.adopt((String) Prompt.prompt("Please enter your name: ",  input, ""),
									(String) Prompt.prompt("Please enter your pets name: ", input, ""));
		Print.ln("My records on your pet are inaccurate.\nHere is what they currently say:");
		myPet.writeOutput();
		myPet.setAge((int) Prompt.prompt("Please enter the correct pet age: ", input, 1));
		myPet.setWeight((double) Prompt.prompt("Please enter the correct pet weight: ", input, new Double("1.0")));
		Print.ln("My updated records now say:");
		myPet.writeOutput();
		input.close();
	}
}

class Pet {
	private String ownerName;
	private String name;
	private int age;
	private double weight;
	public Pet() {
		this("No owner yet", "No name yet", 0, 0);
	}
	public Pet(String ownerName, String name, int age, double weight) {
		set(ownerName, name, age, weight);
	}
	public Pet(String ownerName, String name) {
		this(ownerName, name, 0, 0);
	}
	public Pet(String name) {
		this("No owner yet", name, 0, 0);
	}
	public Pet(String name, int age) {
		this("No owner yet", name, age, 0);
	}
	public Pet(String name, double weight) {
		this("No owner yet", name, 0, weight);
	}
	public Pet(String name, int age, double weight) {
		this("No owner yet", name, age, weight);
	}
	public Pet(int age) {
		this("No owner yet", "No name yet", age, 0);
	}
	public Pet(double weight) {
		this("No owner yet", "No name yet", 0, weight);
	}
	public Pet adopt(String ownerName, String name) {
		return new Pet(ownerName, name, age, weight);
	}
	private void set(String ownerName, String name, int age, double weight) {
		setOwnerName(ownerName);
		setName(name);
		setAge(age);
		setWeight(weight);
	}
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setAge(int age) {
		if(age < 0) { 
			ErrPrint.ln("Error: Negative age.");
			System.exit(0);
		} else {
			this.age = age;
		}
	}
	public void setWeight(double weight) {
		if(weight < 0) { 
			ErrPrint.ln("Error: Negative weight.");
			System.exit(0);
		} else {
			this.weight = weight;
		}
	}
	public String getOwnerName() {
		return ownerName;
	}
	public String getName() {
		return this.name;
	}
	public int getAge() {
		return this.age;
	}
	public double getWeight() {
		return this.weight;
	}
	public void writeOutput() {
		Print.ln("Owner Name: " + this.ownerName + "\n" +
					"Name: " + this.name + "\n" +
					"Age: " + this.age + " years\n" +
					"Weight: " + this.weight + " pounds");
	}
}
class Dog extends Pet {
	public final String species = "Dog";
	public Dog() {
		this("No owner yet", "No name yet", 0, 0);
	}
	public Dog(String ownerName, String name, int age, double weight) {
		super(ownerName, name, age, weight);
	}
	public Dog(String ownerName, String name) {
		this(ownerName, name, 0, 0);
	}
	public Dog(String name) {
		this("No owner yet", name, 0, 0);
	}
	public Dog(String name, int age) {
		this("No owner yet", name, age, 0);
	}
	public Dog(String name, double weight) {
		this("No owner yet", name, 0, weight);
	}
	public Dog(String name, int age, double weight) {
		this("No owner yet", name, age, weight);
	}
	public Dog(int age) {
		this("No owner yet", "No name yet", age, 0);
	}
	public Dog(double weight) {
		this("No owner yet", "No name yet", 0, weight);
	}
	@Override
	public Dog adopt(String ownerName, String name) {
		return new Dog(ownerName, name, getAge(), getWeight());
	}
	public String getSpecies() {
		return this.species;
	}
	@Override
	public void writeOutput() {
		Print.ln("Owner Name: " + getOwnerName() + "\n" +
					"Name: " + getName() + "\n" +
					"Age: " + getAge() + " years\n" +
					"Weight: " + getWeight() + " pounds\n" +
					"Species: " + getSpecies());
	}
}