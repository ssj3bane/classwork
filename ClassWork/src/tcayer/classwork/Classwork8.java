package tcayer.classwork;



public class Classwork8 { 
	public static void main(String[] args) {
		BankAccount myAccount = new BankAccount(100, 5);
		myAccount.showNewBalance();
		myAccount.wishfulThinking(800);
		BankAccount myAccountSecond = new BankAccount(200, 1);
		myAccountSecond.showNewBalance();
	}
}

class BankAccount {
	public double amount;
	public double rate;
	public BankAccount() {}
	public BankAccount(double amount, double rate) {
		this.amount = amount;
		this.rate = rate;
	}
	public void showNewBalance() {
		double newAmount = amount + (rate / 100) * amount;
		System.out.println("With interest added, the new amount is $" + newAmount);
	}
	public void wishfulThinking(double newAmount) {
		System.out.println("I wish my new amount was $" + newAmount);
	}
}