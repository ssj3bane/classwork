package tcayer.classwork;
import java.util.Scanner;

public class Classwork5 {
	public static final double OVERDRAWN_PENALTY = 8.00;
	public static final double INTEREST_RATE = 0.02; //2K Annually
	public static void main(String[] args) {
		double balance;
		
		System.out.print("Enter your checking account balance: $");
		Scanner kb = new Scanner(System.in);
		balance = kb.nextDouble();
		kb.close();
		System.out.println("Original balance $" + balance);
		if(balance >= 0) {
			balance += (INTEREST_RATE * balance) / 12;
		} else {
			balance -= OVERDRAWN_PENALTY;
		}
		System.out.println("After adjusting for one month of interest and penalties,\nyour new balance is $" + balance);
	}
}