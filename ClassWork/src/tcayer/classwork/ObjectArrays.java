package tcayer.classwork;

public class ObjectArrays {
	public static int globalSize = 5;
	public static Object[] mArray = new Object[] {
		createArray(globalSize),
		createArray(globalSize),
		createArray(globalSize),
		createArray(globalSize),
		createArray(globalSize)
	};
	public static void main(String[] args) {
		for(Object piece : mArray) {
			if(piece instanceof Object[]) {
				for(int index = 0; index < ((Object[])piece).length; index++) {
					((Object[])piece)[index] = Math.random();
				}
			}
		}
		loopIfObject(mArray);
	}
	public static void loopIfObject(Object piece) {
		if(piece instanceof Object[]) {
			for(Object subPiece : (Object[])piece) {
				loopIfObject(subPiece);
			}
		} else {
			System.out.println(piece);
		}
	}
	public static Object[] createArray(int length) {
		return new Object[length];
	}
}