package tcayer.classwork;

public class UnitConverter {
	private static final int INCHES_IN_FEET = 12;
	private static final double CM_IN_INCHES = 2.54;
	public static double convertInchesToFeet(double inches) {
		return inches / INCHES_IN_FEET;
	}
	public static double convertFeetToInches(double feet) {
		return feet * INCHES_IN_FEET;
	}
	public static double convertInchesToCm(double inches) {
		return inches * CM_IN_INCHES;
	}
	public static double convertCmToInches(double cm) {
		return cm / CM_IN_INCHES;
	}
}
