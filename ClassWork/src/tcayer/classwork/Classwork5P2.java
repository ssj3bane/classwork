package tcayer.classwork;
import java.util.Scanner;
public class Classwork5P2 {
	public static void main(String[] args) {
		String s1;
		String s2;
		System.out.println("Enter two lines of text:");
		Scanner kb = new Scanner(System.in);
		s1 = kb.nextLine();
		s2 = kb.nextLine();
		kb.close();
		if(s1.equals(s2))
			System.out.println("The two lines are equal.");
		else
			System.out.println("The two lines are not equal.");
		if(s1.equalsIgnoreCase(s2))
			System.out.println("But the lines are equal, ignoring case.");
		else
			System.out.println("Lines are not equal, even ignoring case.");
	}
}