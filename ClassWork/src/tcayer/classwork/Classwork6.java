package tcayer.classwork;

import java.util.Scanner;

public class Classwork6 {
	public static void main(String[] args) {
		int count, number;
		System.out.println("Enter a number");
		Scanner kb = new Scanner(System.in);
		number = kb.nextInt();
		kb.close();
		count = 1;
		do {
			System.out.print(count + ", ");
			count++;
		} while(count <= number);
		System.out.println();
		System.out.println("Buckle my shoe.");
	}
}