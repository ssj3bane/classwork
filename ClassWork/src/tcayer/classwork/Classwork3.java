package tcayer.classwork;

public class Classwork3
{
	public static void main(String[] args)
	{
		System.out.println("#1");
		float x;
		x = (float) 10.5;
		int m;
		m = (int)x;
		System.out.println("m = "+m);
		System.out.println("#2");
		int code = (int) 'A';
		System.out.println("code: " + code);
		System.out.println("#3");
		char letter = (char) 66;
		System.out.println("letter: " + letter);
		System.out.println("#4");
		int sum, count;
		double avg;
		sum = 20;
		count = 8;
		avg = sum/count;
		System.out.println("" + sum + "/" + count + " = " + avg);
		System.out.println("#5");
		avg = (double)sum/(double)count;
		System.out.println("(double)"+sum+"/"+"(double)"+count+" = "+avg);
		System.out.println("#6");
		char small, cap;
		cap = 'A';
		small = (char)((int)cap+32);
		System.out.println("cap: "+cap+", small: "+small);
	}
}