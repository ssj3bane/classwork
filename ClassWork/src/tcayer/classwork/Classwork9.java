package tcayer.classwork;

import java.util.Scanner;

public class Classwork9 {
	public static String name;
	public static int[] SCORES = new int[9]; 
	public static int[] HIGH_SCORES = new int[]{
		-1,
		-1,
		-1,
		-1,
		-1,
		-1
	};
	public static void main(String[] args) {
		Scanner kb = new Scanner(System.in);
		System.out.println("Enter your name");
		name = kb.nextLine();
		System.out.println("Enter your 9 scores");
		for(int i = 0; i < 9; i++)
			SCORES[i] = kb.nextInt();
		int[] lowest3Scores = new int[3];
		for(int i = 0; i < 3; i++) {
			int lowestScore = 100;
			int lowestScoreIndex = 0;
			for(int j = 0; j < 9; j++) {
				if(!arrayContains(lowest3Scores,j)) {
					if(SCORES[j] < lowestScore) {
						lowestScore = SCORES[j];
						lowestScoreIndex = j;
					}
				}
			}
			lowest3Scores[i] = lowestScoreIndex;
		}
		for(int i = 0; i < 9; i++) {
			boolean isLowest = false;
			for(int j = 0; j < 3; j++) {
				if(i == lowest3Scores[j]) {
					isLowest = true;
				}
			}
			if(!isLowest) {
				HIGH_SCORES[nonNullLength(HIGH_SCORES)] = SCORES[i];
			}
		}
		int TOTAL_POINTS = 0;
		for(int i = 0; i < 6; i ++) {
			TOTAL_POINTS += HIGH_SCORES[i];
		}
		System.out.println("Name: " + name + "\nTotal Points: "+TOTAL_POINTS);
		kb.close();
	}
	public static int nonNullLength(int[] array) {
		int rv=0;
		for(int i = 0; i < array.length; i++) {
			if(array[i] >= 0) {
				rv++;
			}
		}
		return rv;
	}
	public static boolean arrayContains(String[] array, String value) {
		boolean rv = false;
		for(String piece : array) {
			if(piece == value) {
				rv = true;
			}
		}
		return rv;
	}
	public static boolean arrayContains(int[] array, int value) {
		boolean rv = false;
		for(int piece : array) {
			if(piece == value) {
				rv = true;
			}
		}
		return rv;
	}
}