package tcayer.classwork;

import java.util.Scanner;

public class Classwork7 {
	public static final int SPENDING_MONEY = 100;
	public static final int MAX_ITEMS = 3;
	public static void main(String[] args) {
		Scanner kb = new Scanner(System.in);
		boolean haveMoney = true;
		int leftToSpend = SPENDING_MONEY;
		int totalSpent = 0;
		int itemNumber = 1;
		while(haveMoney && (itemNumber <= MAX_ITEMS)) {
			System.out.println("You may buy up to " + (MAX_ITEMS - itemNumber + 1) + " items\n"
					+ "costing no more than $" + leftToSpend + ".");
			System.out.print("Enter cost of item #" + itemNumber + ": $");
			int itemCost = kb.nextInt();
			if(itemCost <= leftToSpend) {
				System.out.println("You may buy this item.");
				totalSpent = totalSpent + itemCost;
				System.out.println("You have spent $" + totalSpent + " so far.");
				leftToSpend = SPENDING_MONEY - totalSpent;
				if(leftToSpend > 0) {
					itemNumber++;
				} else {
					System.out.println("You are out of money.");
					haveMoney = false;
				}
			} else {
				System.out.println("You cannot buy that item.");
			}
		}
		System.out.println("You have spent $" + totalSpent + ", and are done shopping.");
		kb.close();
	}
}