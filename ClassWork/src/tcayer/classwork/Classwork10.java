package tcayer.classwork;

public class Classwork10 {
	public static void main(String[] args) {
		Student x = new Student();
		System.out.println(x.series(10, 5));
		System.out.println(x.factorial(5));
	}
}

class Student {
	public String name;
	public Student() {
		
	}
	public int series(int x, int n) {
		int rv=1;
		for(int i = 1; i <= n; i++)
			rv += Math.pow(x, i);
		return rv;
	}
	public int factorial(int x) {
		int rv = x;
		for(int i = x-1; i > 0; i--)
			rv *= i;
		return rv;
	}
}
