package tcayer.classwork;

import java.util.Scanner;

public class Classwork12 {
	public static void main(String[] args) {
		Scanner kb = new Scanner(System.in);
		Print.ln("Result is " + UnitConverter.convertInchesToFeet((double) Prompt.prompt("Enter inches to convert to feet: ", kb, new Double("1.0"))) + " feet.");
		Print.ln("Result is " + UnitConverter.convertInchesToCm((double) Prompt.prompt("Enter inches to convert to cm: ", kb, new Double("1.0"))) + " cm.");
		Print.ln("Result is " + UnitConverter.convertFeetToInches((double) Prompt.prompt("Enter feet to convert to inches: ", kb, new Double("1.0"))) + " inches.");	
		Print.ln("Result is " + UnitConverter.convertCmToInches((double) Prompt.prompt("Enter centimeter to convert to inches: ", kb, new Double("1.0"))) + " inches.");
		kb.close();
	}
}
class Prompt {
	public static Object prompt(String _prompt, Scanner input, Object type) {
		Print.p(_prompt);
		Object rv = new Error();
		boolean t = true;
		if(type instanceof Integer)
			if(input.hasNextInt())
				rv = input.nextInt();
		else if(type instanceof String)
			if(input.hasNext())
				rv = input.next();
		else if(type instanceof Boolean)
			if(input.hasNextBoolean())
				rv = input.nextBoolean();
		else if(type instanceof Float)
			if(input.hasNextFloat())
				rv = input.nextFloat();
		else if(type instanceof Double)
			if(input.hasNextDouble())
				rv = input.nextDouble();
		while(rv instanceof Error && t) {
			if(type instanceof Integer)
				rv = input.nextInt();
			else if(type instanceof String)
				rv = input.next();
			else if(type instanceof Boolean)
				rv = input.nextBoolean();
			else if(type instanceof Float)
				rv = input.nextFloat();
			else if(type instanceof Double)
				rv = input.nextDouble();
			t = false;
		}
		return rv;
	}
}
class Print {
	public static void ln(Object arg0) {
		System.out.println(arg0);
	}
	public static void ln(Object[] arg0) {
		System.out.println(arg0);
	}
	public static void p(Object arg0) {
		System.out.print(arg0);
	}
	public static void p(Object[] arg0) {
		System.out.print(arg0);
	}
}
