package tcayer.softwaredesign;

import java.util.Scanner;

public class Reverse {
	public static int[] array = new int[12];
	public static void main(String[] args) {
		Scanner kb = new Scanner(System.in);
		for(int i = 0; i < array.length; i++) {
			int num = kb.nextInt();
			array[i] = num;
		}
		for(int i = array.length-1; i >= 0; i--) {
			System.out.println(array[i]);
		}
		kb.close();
	}
}