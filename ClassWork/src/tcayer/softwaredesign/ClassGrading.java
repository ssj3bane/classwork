package tcayer.softwaredesign;

import java.util.Scanner;
import tcayer.improvements.Print;

public class ClassGrading {
	private static int[] GRADES;
	public static void main(String[] args) {
		input();
		print();
	}
	public static void input() {
		Scanner kb = new Scanner(System.in);
		Print.ln("How many grades will you be inputing?");
		GRADES = new int[kb.nextInt()];
		Print.ln("You will be prompted " + GRADES.length + " times,\nenter a grade each time.");
		for(int i = 0; i < GRADES.length; i++)
			GRADES[i] = kb.nextInt();
		kb.close();
	}
	public static void print() {
		int avg = 0;
		for(int i = 0; i < GRADES.length; i++)
			avg += GRADES[i];
		avg /= GRADES.length;
		Print.ln("The average is " + avg);
	}
}