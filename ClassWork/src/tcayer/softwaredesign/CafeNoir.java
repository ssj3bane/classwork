package tcayer.softwaredesign;

import java.util.Scanner;
/**
 * @author Timothy Cayer
 * I've  always been better at writing actual code
 * rather than pseudo-code. So I decided to make
 * the program in question. I hope it will suffice to
 * show I have the knowledge-base.
 */
public class CafeNoir {
	public static final int MAX_TRIES = 3;
	public static final int MIN_AGE = 10;
	public static final int MAX_AGE = 110;
	public static final int MIN_ITEMS = 1;
	public static final int MAX_ITEMS = 10;
	public static final int MAX_ZIPCODE = 99999;
	public static final int CAFENOIR_ZIPCODE = 54984;
	public static int[] zipCodeOrderCount = new int[MAX_ZIPCODE+1];
	public static int[] zipCodeOrders = new int[MAX_ZIPCODE+1];
	public static int[] ageOrders = new int[MAX_AGE+1];
	public static int[] ageOrderCount = new int[MAX_AGE+1];
	public static int totalOrders = 0;
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		setup();
		enterOrder(input);
		input.close();
	}
	public static void setup() {
		for(int zCode = 0; zCode < MAX_ZIPCODE; zCode++) {
			zipCodeOrderCount[zCode] = 0;
			zipCodeOrders[zCode] = 0;
		}
		for(int age = MIN_AGE; age <= MAX_AGE; age++) {
			ageOrderCount[age] = 0;
			ageOrders[age] = 0;
		}
	}
	public static void enterOrder(Scanner input) {
		int age;
		int zipCode = 1;
		int itemsOrdered;
		while(zipCode != 0) {
			System.out.println("Zipcode:");
			zipCode = input.nextInt();
			if(zipCode == 0) {
				System.out.println("Done for the day.");
				break;
			} else {
				while(Integer.toString(zipCode).length() > 5) {
					System.out.println("Invalid zipcode...\n"
							+ "Enter a valid zipcode:");
					zipCode = input.nextInt();
				}
				System.out.println("Age:");
				age = input.nextInt();
				while(age > MAX_AGE || age < MIN_AGE) {
					System.out.println("Invalid age...\n"
							+ "Enter a valid age:");
					age = input.nextInt();
				}
				itemsOrdered = promptOrder(input);
				if(itemsOrdered != -1) {
					ageOrders[age] += itemsOrdered;
					ageOrderCount[age]++;
					zipCodeOrders[zipCode] += itemsOrdered;
					zipCodeOrderCount[zipCode]++;
					totalOrders++;
				}
			}
		}
		displaySales(input);
	}
	public static int promptOrder(Scanner input) {
		int returnValue = -1;
		int tryCount = 0;
		while((tryCount < MAX_TRIES) && (returnValue > MAX_ITEMS || returnValue < MIN_ITEMS)) {
			System.out.println("Enter amount of items ordered:");
			returnValue = input.nextInt();
			tryCount++;
		}
		if(tryCount >= MAX_TRIES) {
			if(!(returnValue >= MAX_ITEMS)) {
				returnValue = -1;
			}
		}
		return returnValue;
	}
	public static void displaySales(Scanner input) {
		int _continue_ = 1;
		while(_continue_ == 1) {
			int answer;
			int intAnswer;
			System.out.println("Display all zipcodes? 1/0");
			answer = input.nextInt();
			if(answer == 0) {
				System.out.println("Display which zipcode?");
				intAnswer = input.nextInt();
				int zipCode = intAnswer;
				String displayZipCode = formatZipCode(zipCode);
				System.out.println("Orders for " + displayZipCode + ": " + zipCodeOrders[zipCode]
						+ "\nOrder Count: " + zipCodeOrderCount[zipCode]);
			} else {
				System.out.println("Omitting zipcodes with no orders");
				for(int zipCode = 0; zipCode < MAX_ZIPCODE; zipCode++) {
					if(zipCodeOrders[zipCode] != 0) {
						String displayZipCode = formatZipCode(zipCode);
						System.out.println("Orders for " + displayZipCode + ": " + zipCodeOrders[zipCode]
								+ "\nOrder Count: " + zipCodeOrderCount[zipCode]);
					}
				}
			}
			int totalAge = 0;
			int agesAccountedFor = 0;
			for(int age = MIN_AGE; age <= MAX_AGE; age++) {
				if(ageOrderCount[age] != 0) {
					agesAccountedFor += ageOrderCount[age];
					totalAge += age * ageOrderCount[age];
				}
			}
			if(totalAge != 0 && agesAccountedFor != 0) {
				System.out.println("Average age: " + ((float)((float)totalAge/(float)agesAccountedFor)));
			}
			int itemsOrderedExcept45 = 0;
			for(int age = MIN_AGE; age <= MAX_AGE; age++) {
				if(age != 45) {
					itemsOrderedExcept45 += ageOrders[age];
				}
			}
			System.out.println("Total amount of items sold"
					+ " excluding 45 y/o customers: "
					+ itemsOrderedExcept45);
			System.out.println("Continue? 1/0");
			_continue_ = input.nextInt();
		}
		if(_continue_ == 0) {
			System.out.println("Have a nice day.");
		}
	}
	public static String formatZipCode(int zipCode) {
		String niceZipCode = "";
		int missingZero = 5-Integer.toString(zipCode).length();
		for(int i = 0; i < missingZero; i++) {
			niceZipCode += "0";
		}
		niceZipCode += zipCode;
		return niceZipCode;
	}
}