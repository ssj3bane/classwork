package tcayer.softwaredesign;

import java.util.Scanner;

public class MinMax {
	public static int largestNumber = 0;
	public static int smallestNumber = 0;
	public static int[] array = new int[12];
	public static void main(String[] args) {
		Scanner kb = new Scanner(System.in);
		for(int i = 0; i < array.length; i++) {
			int num = kb.nextInt();
			array[i] = num;
			if(i == 0) {
				smallestNumber = num;
				largestNumber = num;
			} else {
				if(num > largestNumber) {
					largestNumber = num;
				}
				if(num < smallestNumber) {
					smallestNumber = num;
				}
			}
		}
		for(int i = 0; i < array.length; i++) {
			System.out.println(array[i]);
		}
		System.out.println("Smallest: " + smallestNumber);
		System.out.println("Largest: " + largestNumber);
		kb.close();
	}
}

//display()