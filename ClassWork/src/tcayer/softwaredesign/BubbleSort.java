package tcayer.softwaredesign;

import tcayer.improvements.ArrayUtil;;

public class BubbleSort {
	public static String[] names = new String[] {
			"Jone",
			"Alex",
			"Brown",
			"Cindy",
			"Andrew"
	};
	public static void main(String[] args) {
		String[] sortedNames = ArrayUtil.sort(names, 1);
		for(String name : sortedNames) {
			System.out.println(name);	
		}
		sortedNames = ArrayUtil.sort(names, -1);
		for(String name : sortedNames) {
			System.out.println(name);	
		}
	}
}