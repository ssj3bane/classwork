package tcayer.projects.zork;

import tcayer.projects.zork.Input;

public class Zork {
	public static void main(String[] args) {
		Input chat = new Input(System.in);
		chat.getInput("TYPE QUIT TO EXIT\nThis is a test, enter your name:");
		chat.printHistory();
		while(chat.lastCommand != "QUIT") {
			chat.getInput("TYPE QUIT TO EXIT\nThis is a test, enter a command:");
			chat.printHistory();
		}
	}
}