package tcayer.projects.zork;

import java.io.InputStream;
import java.util.Scanner;

public class Input {
	private final String commandSeperator = "__";
	public String lastCommand;
	public String[] commandHistory = new String[256];
	public Scanner scanner;
	public Input(InputStream InStream) {
		scanner = new Scanner(InStream);
	}
	/**
	 * parseLastCommand
	 * This method is where the bulk of the command processing will take place
	 * @return commandString
	 */
	public String parseLastCommand() {
		String commandString = lastCommand;
		String[] commands = commandString.split(commandSeperator);
		return commandString;
	}
	public void updateCommandHistory(String command) {
		int amtOfNonEmptyCommands = 0;
		for(int index = 0; index < commandHistory.length; index++) {
			if(commandHistory[index] != "" && commandHistory[index] != null ) {
				amtOfNonEmptyCommands++;
			}
		}
		for(int index = commandHistory.length-1; index > 0 ; index--) {
			commandHistory[index-1] = commandHistory[index];
		}
		commandHistory[0] = command;
	}
	public void getInput(String prompt) {
		System.out.println(prompt);
		lastCommand = scanner.nextLine();
		updateCommandHistory(lastCommand);
	}
	public void printHistory() {
		int commandIndex = 0;
		for(Object command : commandHistory) {
			if(command != null) {
				System.out.println("Command ID: " + commandIndex + "\n\tCommand: " + command);
				commandIndex++;
			}
		}
	}
}