package tcayer.projects.species;
public class DogDemo {
	public static void main(String[] args) {
		Dog balto = new Dog("Balto", 8, "Siberian Husky");
		balto.writeOutput();
		Dog scooby = new Dog("Scooby", 42, "Great Dane");
		scooby.writeProperOutput();
		scooby.rewriteAge();
		Dog lucy = new Dog();
		lucy.name = "LoLo";
		lucy.writeOutput();
	}
}