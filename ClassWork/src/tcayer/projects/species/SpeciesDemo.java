package tcayer.projects.species;

public class SpeciesDemo {
	public static void main(String[] args) {
		Species speciesOfTheMonth = new Species();
		System.out.println("Enter data on the Species of he Month:");
		speciesOfTheMonth.writeOutput();
		System.out.println("In ten years the population will be " + speciesOfTheMonth.predictPopulation(10));
		System.out.println("In twenty years the population will be " + speciesOfTheMonth.predictPopulation(20));
		//Change the species to show how to change
		//the values of instance variables:
		speciesOfTheMonth.setSpecies("Klingon ox", 10, 15);
		System.out.println("The new Species of the Month:");
		speciesOfTheMonth.writeOutput();
		System.out.println("In ten years the population will be " + speciesOfTheMonth.predictPopulation(10));
		System.out.println("In twenty years the population will be " + speciesOfTheMonth.predictPopulation(20));
		speciesOfTheMonth.predictGenderPopulation(20);
		speciesOfTheMonth.changePopulation(4, 20);
		speciesOfTheMonth.predictGenderPopulation(20);
		speciesOfTheMonth.readInput();
	}
}