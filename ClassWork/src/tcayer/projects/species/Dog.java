package tcayer.projects.species;

import java.util.Scanner;

public class Dog {
	public String name;
	public String breed;
	public int age;
	public Dog() {
		System.out.println("Dog created without initial variables.");
	}
	public Dog(String _name, int _age, String _breed) {
		System.out.println("Dog created with initial variables");
		this.name = _name;
		this.age = _age;
		this.breed  = _breed;
	}
	public void writeOutput() {
		System.out.println("Name: " + name +
			"\nBreed: " + breed +
			"\nAge in calendar years: " + age +
			"\nAge in human years: " + getAgeInHumanYears());
		System.out.println();
	}
	public void writeProperOutput() {
		System.out.println(this.name + " is a " + this.breed + ".");
		System.out.println("He is " + this.age + " years old, or " + this.getAgeInHumanYears() + " in human years.");
	}
	public void rewriteAge() {
		Scanner kb = new Scanner(System.in);
		System.out.println("What is the age of " + this.name + "?");
		this.age = kb.nextInt();
		kb.close();
		System.out.println(this.name + " is now " + this.age + " years old.");
	}
	public int getAgeInHumanYears() {
		int humanAge = 0;
		if (age <= 2) {
			humanAge = age * 11;
		} else {
			humanAge = 22 + ((age-2) * 5);
		}
		return humanAge;
	}
}