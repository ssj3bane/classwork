package tcayer.projects.species;
import java.util.Scanner;
public class Species {
	private String name;
	private int population;
	private double growthRate;
	public Species() {
		
	}
	public Species(String _name, int _pop, double _gRate) {
		this.name = _name;
		this.population = _pop;
		this.growthRate = _gRate;
	}
	public void readInput() {
		Scanner keyboard = new Scanner(System.in);
		System.out.println("What is the species' name?");
		String _name = keyboard.nextLine();
		System.out.println("What is the population of the species?");
		int _population = keyboard.nextInt();
		System.out.println("Enter growth rate (% increase per year):");
		double _growthRate = keyboard.nextDouble();
		keyboard.close();
		setSpecies(_name, _population, _growthRate);
	}
	public void writeOutput() {
		System.out.println("Name: " + name);
		System.out.println("Population: " + population);
		System.out.println("Growth rate: " + growthRate + "%");
	}
	public int predictPopulation(int time) {
		int result = 0;
		double populationAmount = population;
		for(int count = time; count > 0; count--) {
			if(populationAmount > 0) {
				populationAmount = populationAmount + (growthRate / 100) * populationAmount;
			} else {
				break;
			}
		}
		if (populationAmount > 0) {
			result = (int)populationAmount;
		}
		return result;
	}
	public void predictGenderPopulation(int time) {
		int maleResult = 0;
		int femaleResult = 0;
		double populationAmount = population;
		for(int count = time; count > 0; count--) {
			if(populationAmount > 0) {
				populationAmount = populationAmount + (growthRate / 100) * populationAmount;
			} else {
				break;
			}
		}
		if (populationAmount > 0) {
			maleResult = (int)(Math.floor(populationAmount/2));
			femaleResult = (int)(Math.ceil(populationAmount/2));
		}
		System.out.println("In " + time + " years, " +
				"the female population will be " + femaleResult +
				", and the male population " + maleResult);
	}
	public void changePopulation(double area, int numberPerMile) {
		System.out.println("Changing population to " + numberPerMile + 
				", with an area of " + area + " sq. miles.");
		this.population = (int)(area * numberPerMile);
	}
	public void setSpecies(String name, int pop, double gRate) {
		this.name = name;
		if(pop < 0) {
			System.err.println(new Error("Population cannot be negative"));
			System.exit(0);
		}
		this.population = pop;
		this.growthRate = gRate;
	}
	public String getName() {
		return this.name;
	}
	public int getPopulation() {
		return this.population;
	}
	public double getGrowthRate() {
		return this.growthRate;
	}
}