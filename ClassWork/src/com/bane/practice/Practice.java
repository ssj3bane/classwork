package com.bane.practice;

import java.util.Scanner;
@SuppressWarnings("unused")
public class Practice {
	public static void main(String[] args) {
		Scanner kb = new Scanner(System.in);
		System.out.println("x+...x^n; Enter x, then n");
		System.out.println(sumPow(kb.nextInt(), kb.nextInt()));
		System.out.println("x!; Enter x");
		System.out.println(factorial(kb.nextInt()));
		kb.close();
	}
	public static int sumPow(int x, int n) {
		int rv=1;
		for(int i = 1; i <= n; i++)
			rv += Math.pow(x, i);
		return rv;
	}
	public static int factorial(int x) {
		int rv = x;
		for(int i = x-1; i > 0; i--)
			rv *= i;
		return rv;
	}
}